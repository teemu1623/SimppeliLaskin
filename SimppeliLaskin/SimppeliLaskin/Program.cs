﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimppeliLaskin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("SimppeliLaskin 1.3v\n");

            Valinta:
            Console.WriteLine("\nValitse laskutyyppi:\n\n1.Summa\n2.Erotus\n3.Kerto\n4.Osamäärä");
            int Laskutyyppi;
            Laskutyyppi = int.Parse(Console.ReadLine());

            if (Laskutyyppi == 1)
            {
                Console.WriteLine("\nAnna lukuja jotka haluat laskea yhteen. Anna luku 0 lopuksi.");
                int i = 1, y = 0;
                while (i !=0 )
                {
                    i = int.Parse(Console.ReadLine());
                    y += i;
                }
                Console.WriteLine("Antamiesi lukujen summa on: " + y + "\n");
            }

            else if (Laskutyyppi == 2)
            {
                Console.WriteLine("\nAnna lukuja jotka haluat vähentää toisistaan. Anna luku 0 lopuksi.");
                int i = 1, y;
                y = int.Parse(Console.ReadLine());

                while (i != 0)
                {
                    i = int.Parse(Console.ReadLine());
                    y -= i;
                }
                Console.WriteLine("Antamiesi lukujen erotus on: " + y + "\n");
            }

            else if (Laskutyyppi == 3)
            {
                Console.WriteLine("\nAnna lukuja jotka haluat kertoa keskenään. Anna luku 0 lopuksi.");
                int i = 1, y;
                y = int.Parse(Console.ReadLine());

                while (i != 0)
                {
                    i = int.Parse(Console.ReadLine());
                    if (i != 0) y = y*i;
                }
                Console.WriteLine("Antamiesi lukujen tulo on: " + y + "\n");
            }
            else if (Laskutyyppi == 4)
            {
                Console.WriteLine("\nAnna lukuja jotka haluat jakaa keskenään. Anna luku 0 lopuksi.");
                int i = 1, y;
                y = int.Parse(Console.ReadLine());

                while (i != 0)
                {
                    i = int.Parse(Console.ReadLine());
                    if (i != 0) y = y / i;
                }
                Console.WriteLine("Antamiesi lukujen osamäärä on: " + y + "\n");
            }
            else
            {
                Console.WriteLine("\nVirhe. Valitse luku 1-4\n");
                goto Valinta;
            }
            Loppu:
            Console.WriteLine("\nUusi lasku?\n\n1.Kyllä\n2.Ei");
            int LoppuValinta = 0;
            LoppuValinta = int.Parse(Console.ReadLine());
            if (LoppuValinta == 1) goto Valinta;
            else if (LoppuValinta == 2) Console.WriteLine("\nNäkemiin!\n");
            else
            {
                Console.WriteLine("\nVirhe. Valitse 1 tai 2.\n");
                goto Loppu;
            }
        }
    }
}
